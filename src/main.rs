// ***** Исполняемый файл
mod house;

use crate::house::{
    device::device::{SmartSocket, SmartThermometer},
    provider::provider::{BorrowingDeviceInfoProvider, OwningDeviceInfoProvider},
    smarthouse::{House, Room},
};

fn main() {
    // Инициализация устройств
    let socket1 = SmartSocket::new();
    let thermo2 = SmartThermometer::new();
    let socket3 = SmartSocket::new();
    let thermo4 = SmartThermometer::new();

    let mut kitchen = Room::new("kitchen");
    let mut bedroom = Room::new("bedroom");

    kitchen.devices.insert(socket1.name.clone());
    kitchen.devices.insert(thermo2.name.clone());
    bedroom.devices.insert(socket3.name.clone());
    bedroom.devices.insert(thermo4.name.clone());

    let mut home = House::new("Большой дом");

    home.rooms.insert(kitchen.name.clone(), kitchen);
    home.rooms.insert(bedroom.name.clone(), bedroom);

    let is_exist_device_in_rooms = home.is_exist_device_into_rooms(socket1.name.clone().as_str());
    println!(
        "Такое устройство в комнатах {:?} есть: {is_exist_device_in_rooms}",
        socket1.name.clone().as_str()
    );

    let is_drop_dev = home.drop_device_from_room("kitchen", socket1.name.clone().as_str());
    println!("устройство удалено: {:?}", is_drop_dev);

    let is_exist_device_in_rooms = home.is_exist_device_into_rooms(socket1.name.clone().as_str());
    println!(
        "Такое устройство в комнатах {:?} есть: {is_exist_device_in_rooms}",
        socket1.name.clone().as_str()
    );

    let set_device_into_room = home.insert_device_into_room("bedroom", socket1.name.as_str());
    println!(
        "устройство установлено: {:?}",
        set_device_into_room
    );

    let is_exist_device_in_rooms = home.is_exist_device_into_rooms(socket1.name.as_str());
    println!(
        "Такое устройство в комнатах {:?} есть: {is_exist_device_in_rooms}",
        socket1.name.as_str()
    );

    println!("Комнаты (название): {:?}", home.get_rooms().unwrap());

    for room in home.get_rooms().unwrap() {
        println!("\tКомната: {:?}", room);
        for dev in home.devices(room).unwrap() {
            println!("\t\t\tУстройство: {:?}", dev);
        }
    }

    // Строим отчёт с использованием `OwningDeviceInfoProvider`.
    let info_provider_1 = OwningDeviceInfoProvider { socket: socket1 };

    let report1 = home.create_report(&info_provider_1).unwrap();

    // Строим отчёт с использованием `BorrowingDeviceInfoProvider`.
    let info_provider_2 = BorrowingDeviceInfoProvider {
        socket: &socket3,
        thermo: &thermo4,
    };

    let report2 = home.create_report(&info_provider_2).unwrap();

    // Выводим отчёты на экран:
    println!("Report #1: {:?}", report1);
    println!("Report #2: {:?}", report2);
    println!("Report #2: {:?}", report2.len());

    let duplicate = Room::new("kitchen");
    let is_exist = home.is_exist_room(duplicate.name.as_str());
    println!("Duplicate: {is_exist}");

    let non_duplicate = Room::new("kitchen_");
    let adding = home.add_room(non_duplicate);
    println!("add room: {adding}");

    let removing = home.drop_room("kitchen__");
    println!("remove room: {removing}");

    let removing = home.drop_room("kitchen_");
    println!("remove room: {removing}");
}
