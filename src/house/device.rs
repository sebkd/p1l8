/// модуль устройств

pub mod device {
    use std::borrow::Borrow;
    use uuid::Uuid;

    // Пользовательские устройства:
    #[derive(Hash, Eq, PartialEq, Debug)]
    pub(crate) struct SmartSocket {
        pub(crate) name: String,
        state: bool,
    }

    impl SmartSocket {
        pub(crate) fn new() -> Self {
            SmartSocket {
                name: Uuid::new_v4().to_string(),
                state: false,
            }
        }
    }

    #[derive(Hash, Eq, PartialEq, Debug)]
    pub(crate) struct SmartThermometer {
        pub(crate) name: String,
        state: bool,
    }

    impl SmartThermometer {
        pub(crate) fn new() -> Self {
            SmartThermometer {
                name: Uuid::new_v4().to_string(),
                state: false,
            }
        }
    }

    pub(crate) trait SmartDevice {
        fn name(&self) -> &str;

        fn state(&self) -> &str;

        fn title(&self) -> String;
    }

    impl SmartDevice for SmartSocket {
        fn name(&self) -> &str {
            self.name.borrow()
        }

        fn state(&self) -> &str {
            match self.state {
                true => "включено",
                false => "выключено",
            }
        }

        fn title(&self) -> String {
            let new_string = "SmartSocket...".to_owned() + self.name.borrow();
            new_string
        }
    }

    impl SmartDevice for SmartThermometer {
        fn name(&self) -> &str {
            self.name.borrow()
        }

        fn state(&self) -> &str {
            match self.state {
                true => "включено",
                false => "выключено",
            }
        }

        fn title(&self) -> String {
            let new_string = "SmartThermometer...".to_owned() + self.name.borrow();
            new_string
        }
    }

    pub enum Device {
        SmartSocket,
        SmartThermometer,
    }
}

#[cfg(test)]
mod tests {
    use std::any::Any;
    use crate::house::device::device::{SmartDevice, SmartSocket, SmartThermometer};

    #[test]
    fn test_create_device() {
        //Тест проверки создания устройств
        let socket1 = SmartSocket::new();
        let thermo2 = SmartThermometer::new();

        let excepted_value = vec!["выключено"];

        assert!(!&socket1.name().is_empty());
        assert_eq!(&excepted_value[0], &socket1.state());
        assert!(!&thermo2.name().is_empty());
        assert_eq!(&excepted_value[0], &thermo2.state());
    }
}
