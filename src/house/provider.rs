/// Модуль устройств
pub mod provider {
    use crate::house::device::device::{SmartDevice, SmartSocket, SmartThermometer};
    use std::error::Error;

    pub(crate) trait DeviceInfoProvider {
        fn device_info(&self) -> Result<String, Box<dyn Error>>;
    }

    // Пользовательские поставщики информации об устройствах.
    // Могут как хранить устройства, так и заимствывать.
    pub(crate) struct OwningDeviceInfoProvider {
        pub(crate) socket: SmartSocket,
    }

    impl DeviceInfoProvider for OwningDeviceInfoProvider {
        fn device_info(&self) -> Result<String, Box<dyn Error>> {
            let data = format!("{} : {}", &self.socket.title(), &self.socket.state());
            match Option::from(data) {
                Some(value) => Ok(value),
                None => Err(Box::new("Error provider")).unwrap(),
            }
        }
    }

    pub(crate) struct BorrowingDeviceInfoProvider<'a, 'b> {
        pub(crate) socket: &'a SmartSocket,
        pub(crate) thermo: &'b SmartThermometer,
    }

    impl DeviceInfoProvider for BorrowingDeviceInfoProvider<'_, '_> {
        fn device_info(&self) -> Result<String, Box<dyn Error>> {
            let data = format!(
                "{} : {}; {} : {}",
                &self.socket.title(),
                &self.socket.state(),
                &self.thermo.title(),
                &self.thermo.state()
            );
            match Option::from(data) {
                Some(value) => Ok(value),
                None => Err(Box::new("Error provider")).unwrap(),
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::house::device::device::{SmartDevice, SmartSocket, SmartThermometer};
    use crate::house::provider::provider::{
        BorrowingDeviceInfoProvider, DeviceInfoProvider, OwningDeviceInfoProvider,
    };

    #[test]
    fn test_device_info_owning() {
        // Тест функции device_info структуры OwningDeviceInfoProvider
        let socket1 = SmartSocket::new();
        let excepted = format!("{} : {}", &socket1.title(), &socket1.state());
        let info_provider = OwningDeviceInfoProvider { socket: socket1 };

        assert_eq!(&excepted, &info_provider.device_info().unwrap());
    }

    #[test]
    fn test_device_info_borrow() {
        // Тест функции device_info структуры BorrowingDeviceInfoProvider
        let socket1 = SmartSocket::new();
        let thermo2 = SmartThermometer::new();
        let info_provider = BorrowingDeviceInfoProvider {
            socket: &socket1,
            thermo: &thermo2,
        };
        let excepted = vec![
            format!("{} : {}", &socket1.title(), &socket1.state()),
            format!("{} : {}", &thermo2.title(), &thermo2.state()),
        ];

        assert!(&info_provider.device_info().unwrap().contains(&excepted[0]));
        assert!(&info_provider.device_info().unwrap().contains(&excepted[1]));
    }
}
