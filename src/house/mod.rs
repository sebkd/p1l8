/// библиотека умного дома
pub(crate) mod device;
pub(crate) mod provider;

pub mod smarthouse {
    use crate::house::provider::provider::DeviceInfoProvider;
    use std::collections::{HashMap, HashSet};
    use std::error::Error;
    use uuid::Uuid;

    #[derive(Clone)]
    pub(crate) struct Room {
        pub(crate) devices: HashSet<String>,
        guid: String,
        pub(crate) name: String,
    }

    impl Room {
        pub(crate) fn new(name: &str) -> Self {
            Room {
                devices: HashSet::new(),
                guid: Uuid::new_v4().to_string(),
                name: name.to_string(),
            }
        }
    }

    #[derive(Clone)]
    pub(crate) struct House {
        pub(crate) name: String,
        pub(crate) rooms: HashMap<String, Room>,
        guid: String,
    }

    impl House {
        pub(crate) fn new(name: &str) -> Self {
            House {
                name: name.to_string(),
                rooms: HashMap::new(),
                guid: Uuid::new_v4().to_string(),
            }
        }

        pub(crate) fn get_rooms(&self) -> Result<Vec<&str>, Box<dyn Error>> {
            // Размер возвращаемого массива можно выбрать самостоятельно
            match Option::from(
                self.rooms
                    .keys()
                    .clone()
                    .map(|x| x as &str)
                    .collect::<Vec<&str>>(),
            ) {
                Some(value) => Ok(value),
                None => Err(Box::new("Errors collect data devices")).unwrap(),
            }
        }

        pub(crate) fn is_exist_room(&self, room_name: &str) -> bool {
            !matches!(
                self.get_rooms()
                    .unwrap()
                    .iter()
                    .filter(|&x| *x == room_name)
                    .clone()
                    .map(|x| x as &str)
                    .collect::<Vec<&str>>()
                    .len(),
                0
            )
        }

        pub(crate) fn add_room(&mut self, room: Room) -> bool {
            match self.is_exist_room(room.name.as_str()) {
                true => false,
                false => {
                    self.rooms.insert(room.name.to_string(), room);
                    true
                }
            }
        }

        pub(crate) fn drop_room(&mut self, room_name: &str) -> bool {
            match self.is_exist_room(room_name) {
                true => {
                    self.rooms.remove(room_name).unwrap();
                    true
                }
                false => false,
            }
        }

        pub(crate) fn is_exist_device_into_room(&self, room_name: &str, device_name: &str) -> bool {
            self.rooms
                .get(room_name)
                .unwrap()
                .devices
                .get(device_name)
                .is_some()
        }

        pub(crate) fn is_exist_device_into_rooms(&self, device_name: &str) -> bool {
            for room in &self.rooms {
                match room.1.devices.get(device_name) {
                    None => {}
                    Some(_) => {
                        return true;
                    }
                }
            }
            false
        }

        pub(crate) fn is_exist_room_and_device(
            &self,
            room_name: &str,
            device_name: &str,
        ) -> bool {
            match self.is_exist_device_into_rooms(device_name) {
                false => return false,
                true => {}
            };

            match self.is_exist_room(room_name) {
                false => return false,
                true => {}
            };

            true
        }

        pub(crate) fn drop_device_from_room(
            &mut self,
            room_name: &str,
            device_name: &str,
        ) -> bool {
            return match self
                .is_exist_room_and_device(room_name, device_name)
            {
                false => false,
                true => {
                    let res = self
                        .rooms
                        .get_mut(room_name)
                        .unwrap()
                        .devices
                        .remove(device_name);

                    res
                }
            };
        }

        pub(crate) fn insert_device_into_room(
            &mut self,
            room_name: &str,
            device_name: &str,
        ) -> bool {
            Uuid::parse_str(device_name).
                expect("device name is not UUID");
            return match self.is_exist_room(room_name) {
                true => match self.is_exist_device_into_rooms(device_name) {
                    false => {
                        let res = self
                            .rooms
                            .get_mut(room_name)
                            .unwrap()
                            .devices
                            .insert(device_name.to_string());

                        res
                    }
                    true => false
                },
                false => false,
            }
        }

        pub(crate) fn devices(&self, room: &str) -> Result<Vec<&str>, Box<dyn Error>> {
            // Размер возвращаемого массива можно выбрать самостоятельно
            match Option::from(
                self.rooms
                    .get(room)
                    .unwrap()
                    .devices
                    .iter()
                    .clone()
                    .map(|x| x as &str)
                    .collect::<Vec<&str>>(),
            ) {
                Some(value) => Ok(value),
                None => Err(Box::new("Errors collect data devices")).unwrap(),
            }
        }

        pub(crate) fn create_report(
            &self,
            info: &impl DeviceInfoProvider,
        ) -> Result<String, Box<dyn Error>> {
            match Option::from(info.device_info().unwrap()) {
                Some(value) => Ok(value),
                None => Err(Box::new("Errors create report")).unwrap(),
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::house::device::device::{SmartDevice, SmartSocket, SmartThermometer};
    use crate::house::provider::provider::{BorrowingDeviceInfoProvider, OwningDeviceInfoProvider};
    use crate::house::smarthouse::{House, Room};

    #[test]
    fn test_smarthouse() {
        // Проверка создания структуры SmartHouse
        let mut house = House::new("MyХаус");

        assert_eq!("MyХаус", house.name);
    }

    #[test]
    fn test_smarthouse_get_rooms() {
        // Проверка функции get_rooms
        let socket1 = SmartSocket::new();
        let thermo2 = SmartThermometer::new();
        let socket3 = SmartSocket::new();
        let thermo4 = SmartThermometer::new();

        let mut kitchen = Room::new("kitchen");
        let mut bedroom = Room::new("bedroom");

        kitchen.devices.insert(socket1.name.clone());
        kitchen.devices.insert(thermo2.name.clone());
        bedroom.devices.insert(socket3.name.clone());
        bedroom.devices.insert(thermo4.name.clone());

        let mut home = House::new("Большой дом");

        home.rooms.insert("kitchen".to_string(), kitchen);
        home.rooms.insert("bedroom".to_string(), bedroom);

        let excepted_rooms = vec!["kitchen", "bedroom"];

        assert!(home.get_rooms().unwrap().contains(&excepted_rooms[0]));
        assert!(home.get_rooms().unwrap().contains(&excepted_rooms[1]));
    }

    #[test]
    fn test_smarthouse_devices() {
        // Проверка функции devices
        let socket1 = SmartSocket::new();
        let thermo2 = SmartThermometer::new();

        let mut kitchen = Room::new("kitchen");
        kitchen.devices.insert(socket1.name.clone());
        kitchen.devices.insert(thermo2.name.clone());

        let mut home = House::new("MyХаус");
        let excepted_devices = vec!["SA01", "SA02"];

        home.rooms.insert("kitchen".to_string(), kitchen);

        assert!(home
            .devices("kitchen")
            .unwrap()
            .contains(&&*socket1.name));
        assert!(home
            .devices("kitchen")
            .unwrap()
            .contains(&&*thermo2.name));
    }

    #[test]
    fn test_smarthouse_create_report() {
        // Проверка функции create_report
        let socket1 = SmartSocket::new();
        let thermo2 = SmartThermometer::new();
        let socket3 = SmartSocket::new();
        let mut kitchen = Room::new("kitchen");
        let mut bedroom = Room::new("bedroom");
        let mut home = House::new("MyХаус");

        kitchen.devices.insert(socket1.name.clone());
        kitchen.devices.insert(thermo2.name.clone());
        bedroom.devices.insert(socket3.name.clone());
        home.rooms.insert("kitchen".to_string(), kitchen);
        home.rooms.insert("bedroom".to_string(), bedroom);

        let excepted_report1 = format!(
            "{} : {}", &socket1.title().clone(), &socket1.state().clone());
        let excepted_report2 = vec![
            format!(
                "{} : {}", &socket3.title().clone(), &socket3.state().clone()),
            format!(
                "{} : {}", &thermo2.title().clone(), &thermo2.state().clone()),
        ];

        // Строим отчёт с использованием `OwningDeviceInfoProvider`.
        let info_provider_1 = OwningDeviceInfoProvider { socket: socket1 };

        let report1 = home.create_report(&info_provider_1).unwrap();

        // Строим отчёт с использованием `BorrowingDeviceInfoProvider`.
        let info_provider_2 = BorrowingDeviceInfoProvider {
            socket: &socket3,
            thermo: &thermo2,
        };

        let report2 = home.create_report(&info_provider_2).unwrap();

        assert_eq!(excepted_report1, report1);
        assert!(report2.contains(&excepted_report2[0]));
        assert!(report2.contains(&excepted_report2[1]));
    }

    #[test]
    fn test_smarthouse_is_exist_room() {
        let mut kitchen = Room::new("kitchen");
        let mut home = House::new("Большой дом");

        home.rooms.insert(kitchen.name.clone(), kitchen);

        assert!(home.is_exist_room("kitchen"))
    }

    #[test]
    fn test_smarthouse_is_exist_device_into_room() {
        let thermo2 = SmartThermometer::new();
        let socket1 = SmartSocket::new();

        let mut kitchen = Room::new("kitchen");
        let mut bedroom = Room::new("bedroom");
        let mut home = House::new("Большой дом");

        kitchen.devices.insert(socket1.name.clone());
        bedroom.devices.insert(thermo2.name.clone());

        home.rooms.insert(kitchen.name.clone(), kitchen);
        home.rooms.insert(bedroom.name.clone(), bedroom);

        assert!(home.is_exist_device_into_room("kitchen", socket1.name.as_str()));
        assert!(!home.is_exist_device_into_room("kitchen", thermo2.name.as_str()))
    }

    #[test]
    fn test_smarthouse_is_exist_device_into_rooms() {
        let thermo2 = SmartThermometer::new();
        let socket1 = SmartSocket::new();
        let socket3 = SmartSocket::new();

        let mut kitchen = Room::new("kitchen");
        let mut bedroom = Room::new("bedroom");
        let mut home = House::new("Большой дом");

        kitchen.devices.insert(socket1.name.clone());
        bedroom.devices.insert(thermo2.name.clone());

        home.rooms.insert(kitchen.name.clone(), kitchen);
        home.rooms.insert(bedroom.name.clone(), bedroom);

        assert!(home.is_exist_device_into_rooms(socket1.name.as_str()));
        assert!(home.is_exist_device_into_rooms(thermo2.name.as_str()));
        assert!(!home.is_exist_device_into_rooms(socket3.name.as_str()));
    }

    #[test]
    fn test_smarthouse_is_exist_room_and_device() {
        let thermo2 = SmartThermometer::new();
        let socket1 = SmartSocket::new();
        let socket3 = SmartSocket::new();

        let mut kitchen = Room::new("kitchen");
        let mut bedroom = Room::new("bedroom");
        let mut home = House::new("Большой дом");

        kitchen.devices.insert(socket1.name.clone());
        bedroom.devices.insert(thermo2.name.clone());

        home.rooms.insert(kitchen.name.clone(), kitchen);
        home.rooms.insert(bedroom.name.clone(), bedroom);

        assert!(home.is_exist_room_and_device("kitchen", socket1.name.as_str()));
        assert!(home.is_exist_room_and_device("kitchen", thermo2.name.as_str()));
        assert!(!home.is_exist_room_and_device("kitchen", socket3.name.as_str()));
    }

    #[test]
    fn test_smarthouse_add_room() {
        let mut kitchen = Room::new("kitchen");
        let mut bedroom = Room::new("bedroom");
        let mut home = House::new("Большой дом");

        assert!(home.add_room(bedroom));
        assert!(home.add_room(kitchen.clone()));
        assert!(!home.add_room(kitchen));
    }

    #[test]
    fn test_smarthouse_drop_room() {
        let mut bedroom = Room::new("bedroom");
        let mut home = House::new("Большой дом");

        home.add_room(bedroom);

        assert!(home.drop_room("bedroom"));
        assert!(!home.drop_room("bedroom"));
    }

    #[test]
    fn test_smarthouse_insert_device_into_room() {
        let socket1 = SmartSocket::new();
        let socket2 = SmartSocket::new();
        let bedroom = Room::new("bedroom");
        let mut home = House::new("Большой дом");

        home.add_room(bedroom);

        assert!(home.insert_device_into_room("bedroom", socket1.name().clone()));
        assert!(!home.insert_device_into_room("bedroom", socket1.name().clone()));
        assert!(!home.insert_device_into_room("kitchen", socket2.name().clone()));
    }

    #[test]
    fn test_smarthouse_drop_device_from_room() {
        let socket1 = SmartSocket::new();
        let socket2 = SmartSocket::new();
        let bedroom = Room::new("bedroom");
        let mut home = House::new("Большой дом");

        home.add_room(bedroom);
        home.insert_device_into_room("bedroom", socket1.name().clone());


        assert!(home.drop_device_from_room("bedroom", socket1.name().clone()));
        assert!(!home.drop_device_from_room("bedroom", socket1.name().clone()));
        assert!(!home.drop_device_from_room("kitchen", socket2.name().clone()));
    }
}
